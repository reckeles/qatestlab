package auto_answering;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

/**
 * Created by User on 10/7/2017.
 */
public class Logger implements WebDriverEventListener {

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {

    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {

    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {

    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {

    }

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        System.out.println(s + " is going to be navigated");
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        System.out.println(s + " was navigated");
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        System.out.println("The page is going to be navigated back");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        System.out.println("The page is successfully navigated back");
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        System.out.println("The page is going to be navigated forward");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        System.out.println("The page is successfully navigated forward");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        System.out.println("The page is going to be refreshed");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        System.out.println("The page is successfully refreshed");
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Search for element: " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println(by.toString() + " was successfully found");
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {

    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println(webElement.toString() + " is clicked");
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        System.out.println("Fill input " + webElement.toString() + " with value " + charSequences.toString());
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        System.out.println("Data is changed");
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        System.out.println("Script is working");
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        System.out.println("Script is done");
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        System.out.println("The exception was thrown");
    }
}
