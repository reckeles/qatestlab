package auto_answering;


import org.apache.camel.main.Main;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

//import org.openqa.selenium;


/**
 * A Camel Application
 */
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */

    public static void login (EventFiringWebDriver driver){
        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passField = driver.findElement(By.id("passwd"));
        WebElement submitButton = driver.findElement(By.name("submitLogin"));

        emailField.sendKeys("webinar.test@gmail.com");
        passField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        submitButton.click();
    }

    public static void openCategory(EventFiringWebDriver driver, Actions action, WebDriverWait wait){

        WebElement categotyItem = driver.findElement(By.id("subtab-AdminCatalog"));
        action.moveToElement(categotyItem).build().perform();

        WebElement categorySubItem = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("subtab-AdminCategories")));
        categorySubItem.click();
    }

    public  static void addCategory (EventFiringWebDriver driver, JavascriptExecutor jse, String name){
        WebElement addCategoryButton = driver.findElement(By.id("page-header-desc-category-new_category"));
        addCategoryButton.click();

        WebElement nameField = driver.findElement(By.id("name_1"));
        nameField.sendKeys(name);

        jse.executeScript("document.getElementById('category_form_submit_btn').scrollIntoView(true);");

        WebElement saveButton = driver.findElement(By.id("category_form_submit_btn"));
        saveButton.click();
    }

    public static void findAddedCategory(EventFiringWebDriver driver, WebDriverWait wait, String name){

        WebElement nameCategoryField = driver.findElement(By.name("categoryFilter_name"));
        nameCategoryField.sendKeys(name);

        WebElement findButon = driver.findElement(By.id("submitFilterButtoncategory"));
        findButon.click();

        WebElement resultText = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody/tr[1]/td[3]")));
        String result = resultText.getText();

        Assert.assertEquals(result, name);
    }

    @Test
    public static void task1(EventFiringWebDriver driver, WebDriverWait wait, Actions action, JavascriptExecutor jse){
        Scanner in = new Scanner(System.in);
        System.out.println("Enter name of category: ");
        String newCategoryName = in.next();

        login(driver);
        openCategory(driver, action, wait);
        addCategory(driver, jse, newCategoryName);
        findAddedCategory(driver, wait, newCategoryName);
    }

    public static WebDriver chooseBrowser(String nameBrowser){
        if (nameBrowser == "Firefox"){
            FirefoxProfile profile = new FirefoxProfile();
            profile.setEnableNativeEvents(true);
            System.setProperty("webdriver.gecko.driver", "lib/geckodriver.exe");
            return new FirefoxDriver(profile);
            //return driver;
        }
        else if (nameBrowser == "IE"){
            DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
            caps.setCapability("ignoreZoomSetting", true);
            System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
            WebDriver driver= new InternetExplorerDriver(caps);
            return driver;
        }
        else if (nameBrowser == "Chrome"){
            System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
            return new ChromeDriver();
            //return driver;
        }
        else {
            System.out.println("No browser was choosed");
            return null;
        }
    }


    public static void main(String... args) throws Exception {

        Main main = new Main();
        main.addRouteBuilder(new MyRouteBuilder());

        String[] namesBrowser = {"Chrome", "Firefox", "IE"};
        for (String nameBrowser : namesBrowser) {
            WebDriver driver = chooseBrowser(nameBrowser);
            EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);

            wrappedDriver.register(new Logger());


            Actions action = new Actions(wrappedDriver);
            WebDriverWait wait = new WebDriverWait(wrappedDriver, 20);
            JavascriptExecutor jse = (JavascriptExecutor) wrappedDriver;

            wrappedDriver.manage().window().maximize();
            wrappedDriver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
            wrappedDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            task1(wrappedDriver, wait, action, jse);

            wrappedDriver.close();
        }

        main.run(args);
    }
}

