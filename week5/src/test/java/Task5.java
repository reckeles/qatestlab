import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 10/17/2017.
 */
public class Task5 {

    RemoteWebDriver driver;
    /*
    @BeforeTest
    public void setPhantomBrowser(){
        System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
        driver = new ChromeDriver();
        //System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
        //driver = new PhantomJSDriver();
    }*/

    @BeforeTest
    public void setGrid() throws MalformedURLException {
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //System.setProperty("phantomjs.binary.path", "lib/phantomjs.exe");
        //driver = new PhantomJSDriver();
    }

    @Test
    public void someFunc() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 20);

        driver.get("http://prestashop-automation.qatestlab.com.ua/ru/");
        WebElement allGoodsLink = driver.findElement(By.xpath("//a[@class='all-product-link pull-xs-left pull-md-right h4']"));
        allGoodsLink.click();

        int randomGoodID = 1 + (int) ( Math.random() * 7);

        WebElement randomGood = driver.findElement(By.xpath("//div[@class='products row']/article["+randomGoodID+"]/" +
                "div/div[@class='product-description']/h1/a"));

        randomGood.click();

        String nameOfGood = driver.findElement(By.xpath("//h1[@class='h1']")).getText();
        String priceOfGood = driver.findElement(By.xpath("//span[@itemprop='price']")).getText();
        String countOfGood = driver.findElement(By.xpath("//input[@id='quantity_wanted']")).getText();

        WebElement addToCart = driver.findElement(By.xpath("//button[@class='btn btn-primary add-to-cart']"));
        addToCart.click();


        WebElement goToCart = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='btn btn-primary']")));
        goToCart.click();

        String nameOfAddedGood = driver.findElement(By.xpath("//div[@class='product-line-info']/a[@class='label']")).getText();
        String priceOfAddedGood = driver.findElement(By.xpath("//div[@class='product-line-grid-body col-md-4 col-xs-8']/div[2]/span")).getText();
        String countOfAddedGood = driver.findElement(By.xpath("//input[@class='js-cart-line-product-quantity form-control']")).getText();

        Assert.assertEquals(nameOfAddedGood, nameOfGood, "Wrong name" );
        Assert.assertEquals(priceOfAddedGood, priceOfGood, "Wrong price" );
        Assert.assertEquals(countOfAddedGood, countOfGood, "Wrong count" );
    }

    public void inputFields(String xpath, String data){
        WebElement firstNameOfCustomer = driver.findElement(By.xpath(xpath));
        firstNameOfCustomer.clear();
        firstNameOfCustomer.sendKeys(data);
    }

    @Test
    public void test2(String firstname, String lastname, String email, String address,String postcode, String city  ){
        WebElement orderingButton = driver.findElement(By.xpath("//div[@class='text-xs-center']/a"));
        orderingButton.click();

        inputFields("//section/div[2]/div/input[@name='firstname']", firstname);
        inputFields("//section/div[3]/div/input[@name='lastname']", lastname);
        inputFields("//section/div[4]/div/input[@name='email']", email);

        driver.findElement(By.xpath("//footer[@class='form-footer clearfix']/button[@class='continue btn btn-primary pull-xs-right']")).click();

        inputFields("//section/div[5]/div/input[@name='address1']", address);
        inputFields("//section/div[7]/div/input[@name='postcode']", postcode);
        inputFields("//section/div[8]/div/input[@name='city']", city);

        driver.findElement(By.xpath("//section[@id='checkout-delivery-step']/h1[@class='step-title h3']")).click();
        driver.findElement(By.xpath("//section[@id='checkout-payment-step']/h1[@class='step-title h3']")).click();


        int randomPaymentOption = (int) ( Math.random() * 2);
        switch (randomPaymentOption){
            case 0:
                driver.findElement(By.xpath("//div[@id='payment-option-1-container']/span/input")).click();
                break;
            case 1:
                driver.findElement(By.xpath("//div[@id='payment-option-2-container']/span/input")).click();
                break;
            default:
                driver.findElement(By.xpath("//div[@id='payment-option-1-container']/span/input")).click();
                break;
        }

        driver.findElement(By.xpath("//div[@class='pull-xs-left']/span/input")).click();

        driver.findElement(By.xpath("//button[@class='btn btn-primary center-block']")).click();

        //PROBLEMSWITHWORD String nameOfAddedGood = driver.findElement(By.xpath("//div[@class='col-sm-4 col-xs-9 details']")).getText();

        String priceOfAddedGood = driver.findElement(By.xpath("//div[@class='col-xs-5 text-sm-right text-xs-left']")).getText();
        String countOfAddedGood = driver.findElement(By.xpath("//div[@class='col-xs-2']")).getText();

        String labelOK = driver.findElement(By.xpath("//h3[@class='h1 card-title']")).getText().replaceAll(" ", "");

        Assert.assertEquals(nameOfAddedGood, nameOfGood, "Wrong name" );
        Assert.assertEquals(priceOfAddedGood, priceOfGood, "Wrong price" );
        Assert.assertEquals(countOfAddedGood, countOfGood, "Wrong count" );
        Assert.assertEquals(labelOK, "Вашзаказподтверждён", "Wrong label" );



        //////////////////ADD FUNC
        //a[@class='nav-link active']
        //div[@class='product-quantities']/span
    }
}
