package auto_answering;


import org.apache.camel.main.Main;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

//import org.openqa.selenium;


/**
 * A Camel Application
 */
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void script1 (WebDriver driver){
        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passField = driver.findElement(By.id("passwd"));
        WebElement submitButton = driver.findElement(By.name("submitLogin"));

        emailField.sendKeys("webinar.test@gmail.com");
        passField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        submitButton.click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement smthButton  = driver.findElement(By.className("employee_avatar_small"));
        smthButton.click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement forLogOut = driver.findElement(By.id("header_logout"));
        forLogOut.click();
    }

    @Test
    public static void script2(WebDriver driver) throws InterruptedException {
        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passField = driver.findElement(By.id("passwd"));
        WebElement submitButton = driver.findElement(By.name("submitLogin"));

        emailField.sendKeys("webinar.test@gmail.com");
        passField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        submitButton.click();

        //выбор обращаться к пунктам меню через айдишник обусловлен очень сложно устроеным меню
        //плохой прием для написания автотестов, но как автоматизировать то меню с через имя класса
        //и сбора всех пунктов я так и не поняла, что бы оно было без костылей и тд
        String[] id_items = {"tab-AdminDashboard", "subtab-AdminParentOrders", "subtab-AdminCatalog",
                "subtab-AdminParentCustomer", "subtab-AdminParentCustomerThreads", "subtab-AdminStats",
                "subtab-AdminParentModulesSf", "subtab-AdminParentThemes", "subtab-AdminParentShipping",
                "subtab-AdminParentPayment", "subtab-AdminInternational", "subtab-ShopParameters",
                "subtab-AdminAdvancedParameters"};

        for (String id_item: id_items){
            try {
                WebElement item = driver.findElement(By.id(id_item));
                item.click();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            } catch (NoSuchElementException ex){
                System.out.println("Can not find menu item");
                List<WebElement> checkList = driver.findElements(By.className(("link-levelone")));
                if (checkList != null) {
                    driver.navigate().back();
                }
            }

            try{
                WebElement title1 = driver.findElement(By.className("page-title"));
                String nameTitle1 = title1.getText();
                System.out.println(nameTitle1);

                driver.navigate().refresh();

                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

                WebElement title2 = driver.findElement(By.className("page-title"));
                String nameTitle2 = title2.getText();

                Assert.assertEquals(nameTitle1, nameTitle2);
            } catch (NoSuchElementException ex){
                System.out.println("Can not find page title");
                List<WebElement> checkList = driver.findElements(By.className(("link-levelone")));
                if (checkList != null) {
                    driver.navigate().back();
                }
            }
        }
    }

    public static void main(String... args) throws Exception {

        Main main = new Main();
        main.addRouteBuilder(new MyRouteBuilder());

        System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

        WebDriver driver;
        driver = new ChromeDriver();

        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        driver.manage().window().maximize();

        script1(driver);
        script2(driver);

        driver.quit();

        main.run(args);
    }
}

