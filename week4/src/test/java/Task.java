import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static jdk.nashorn.internal.objects.ArrayBufferView.length;

/**
 * Created by User on 10/14/2017.
 */
public class Task {
/*
    WebDriver driver;

    String nameOfGood;
    int quantityOfGood;
    double priceOfGood;


    @BeforeTest
    @Parameters({ "typeDriver" })
    public void funcSetDriver(String typeDriver){

            switch(Integer.parseInt(typeDriver)){
                case 1:
                    System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
                    driver = new ChromeDriver();
                    break;
                case 2:
                    FirefoxProfile profile = new FirefoxProfile();
                    profile.setEnableNativeEvents(true);
                    System.setProperty("webdriver.gecko.driver", "lib/geckodriver.exe");
                    driver = new FirefoxDriver(profile);
                    break;
                case 3:
                    DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
                    caps.setCapability("ignoreZoomSetting", true);
                    System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
                    driver= new InternetExplorerDriver(caps);
                    break;
            }



    }

    @BeforeTest (dependsOnMethods = "funcSetDriver")
    public void setParametrs(){
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }


    WebDriver driver ;



    @BeforeTest
    @Parameters({ "typeDriver" })
    public void funcSetDriver(String typeDriver){
            System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
            driver = new ChromeDriver();
            System.out.print(typeDriver);

    }

    @BeforeTest(dependsOnMethods = "funcSetDriver")
    public void se(){
        driver.manage().window().fullscreen();
        System.out.print("GREAT");

    }

    @Test
    public  void sdf(){}

    @AfterTest
    public  void some(){
        driver.quit();
    }


    //@DataProvider(name = "Authentication")

    //public static Object[][] credentials() {
    //    return new Object[][] { { "testuser_1", "ed"}, { "testuser_1", "ewd"}};
    //}

    String nameOfGood;
    int quantityOfGood;
    double priceOfGood;


    @BeforeSuite
    public void ss(){
        System.out.println("Suite");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{{nameOfGood, quantityOfGood, priceOfGood}};
    }

    @BeforeMethod
    public void some(){
        System.out.println("Method");
    }

    @BeforeClass
    public void ss1(){
        //Scanner in = new Scanner(System.in);
        //System.out.println("Enter name of good: ");
        //nameOfGood = in.next();
        nameOfGood = "Good";
        quantityOfGood = 1 + (int) ( Math.random() * 100);

        priceOfGood = 0.1 + Math.random()*100;
    }

    @BeforeTest
    @Parameters({ "typeDriver" })
    public void ssss(String s){
        System.out.println(s);
    }

    @BeforeTest(dependsOnMethods = "ssss")
    public void aaaa(){
        System.out.println("BTest2");
    }

    @Test(dataProvider="getData")
    public void instanceDbProvider(String p2, int p1, double p3) {
        System.out.println("Instance DataProvider Example: Data(" + p1 + ", " + p2 + ")"+ p3);
    }



    @Test(dataProvider="getData")
    public void instanceDbProvider2(String p2, int p1, double p3) {
        System.out.println("222Instance DataProvider Example: Data(" + p1 + ", " + p2 + ")"+ p3);
    }

    @Test
    public void sssssss(){
        System.out.println("Test2");
    }
*/









    WebDriver driver;

    String nameOfGood;
    int quantityOfGood;
    double priceOfGood;


    @BeforeTest
    @Parameters({ "typeDriver" })
    public void funcSetDriver(String typeDriver){
          switch(Integer.parseInt(typeDriver)){
                case 1:
                    System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
                    driver = new ChromeDriver();
                    break;
                case 2:
                    FirefoxProfile profile = new FirefoxProfile();
                    profile.setEnableNativeEvents(true);
                    System.setProperty("webdriver.gecko.driver", "lib/geckodriver.exe");
                    driver = new FirefoxDriver(profile);
                    break;
                case 3:
                    DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
                    caps.setCapability("ignoreZoomSetting", true);
                    System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
                    driver= new InternetExplorerDriver(caps);
                    break;
            }


    }

    @BeforeTest (dependsOnMethods = "funcSetDriver")
    public void setParametrs(){
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @BeforeClass
    public void createDataOfGood(){
        String[] var = {"Good1", "Good2", "Good3", "Good4"};
        int temp = (int)(Math.random() * (length(var)-1));

        nameOfGood = var[temp];
        quantityOfGood = 1 + (int) ( Math.random() * 100);
        priceOfGood = Math.round(100*(0.1 + Math.random()*100))/100;
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{{nameOfGood, quantityOfGood, priceOfGood}};

    }

    @BeforeMethod
    public  void login(){
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        driver.findElement(By.id("email")).sendKeys("webinar.test@gmail.com");
        driver.findElement(By.id("passwd")).sendKeys("Xcg7299bnSmMuRLp9ITw");
        driver.findElement(By.name("submitLogin")).click();
    }

    @BeforeMethod(dependsOnMethods = "login")
    public void openGoodsPage(){
        Actions action = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 20);

        WebElement categotyItem = driver.findElement(By.id("subtab-AdminCatalog"));
        action.moveToElement(categotyItem).build().perform();

        WebElement categorySubItem = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("subtab-AdminProducts")));
        categorySubItem.click();
    }

    @AfterMethod
    public void logout(){
        driver.findElement(By.xpath("//div[@class=\"img-circle person\"]/i")).click();
        driver.findElement(By.id("header_logout")).click();
    }

    @Test(dataProvider="getData")
    public void addGood(String name, int quantity, double price){
        WebDriverWait wait = new WebDriverWait(driver, 20);


        WebElement addGoodButton = driver.findElement(By.id("page-header-desc-configuration-add"));
        addGoodButton.click();
        
        WebElement priceField = driver.findElement(By.id("form_step1_price_ttc_shortcut"));
        priceField.sendKeys(Double.toString(price));

        WebElement quantityField = driver.findElement(By.id("form_step1_qty_0_shortcut"));
        quantityField.sendKeys(Integer.toString(quantity));

        WebElement nameField = driver.findElement(By.id("form_step1_name_1"));
        nameField.sendKeys(name);

        WebElement activateFlag = driver.findElement(By.className("switch-input"));
        activateFlag.click();

        WebElement closeNotification = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.className("growl-close")));
        closeNotification.click();

        WebElement saveGood = driver.findElement(By.cssSelector(".btn.btn-primary.js-btn-save"));
        saveGood.click();

        closeNotification = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.className("growl-close")));
        closeNotification.click();
    }

    @Test(dataProvider="getData")
    public void checkAddedGood(String name, int quantity, double price){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //search
        driver.findElement(By.xpath("//tr[@class='column-filters']/th[3]/input")).clear();
                driver.findElement(By.xpath("//tr[@class='column-filters']/th[3]/input")).sendKeys(name);
        driver.findElement(By.xpath("//tr[@class='column-filters']/th[4]")).click();
        driver.findElement(By.xpath("//tr[@class='column-filters']/th/button[@class='btn btn-primary']")).click();

        //assert if good was added
        WebElement nameOfGoodTable = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-md-12']/table/tbody/tr/td[3]/a")));

        Assert.assertEquals(nameOfGoodTable.getText(), name, "Good was not added");

        //if yes assert if all data displayed correct
        nameOfGoodTable.click();

        SoftAssert assertData = new SoftAssert();

        WebElement nameField = driver.findElement(By.id("form_step1_name_1"));
        assertData.assertEquals(nameField.getText(), name, "Wrong name in form");

        WebElement quantityField = driver.findElement(By.id("form_step1_qty_0_shortcut"));
        assertData.assertEquals(quantityField.getText(), quantity, "Wrong quantity in form");


        WebElement priceField = driver.findElement(By.id("form_step1_price_shortcut"));
        assertData.assertEquals(priceField.getText(), price, "Wrong price in form");

        assertData.assertAll();
    }

}
